#mycrypt extension is not provided with the PHP source since 7.2 ,
#but are instead available through PECL. To install a PECL extension in docker,
#use pecl install to download and compile it, then use docker-php-ext-enable to enable it:



FROM php:8.2-apache

RUN apt-get update && apt-get install -y \
        libapache2-mod-xsendfile \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libwebp-dev \
        zlib1g-dev \
        zip \
        netbase \
        libzip-dev \
        libicu-dev \
        openssh-server \
        libmagickwand-dev \
        wget \
        git \
    && docker-php-ext-install -j$(nproc) iconv \
#    && pecl install mcrypt-1.0.2 \
#    && docker-php-ext-enable mcrypt  \
    && docker-php-ext-configure gd  --with-freetype  --with-jpeg --with-webp \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install mysqli pdo_mysql \
    && docker-php-ext-install zip \
    && docker-php-ext-install exif \
    && docker-php-ext-install sockets \
    && docker-php-ext-install gettext \
#    && pecl install xdebug  \
#    && docker-php-ext-enable xdebug \
    && pecl install redis  \
    && docker-php-ext-enable redis \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-install opcache

ENV APACHE_DOCUMENT_ROOT /var/www/html/public
# DOCUMENT ROOT FOR LARAVEL
RUN sed -ri -e "s!/var/www/html!${APACHE_DOCUMENT_ROOT}!g" /etc/apache2/sites-available/*.conf
RUN sed -ri -e "s!/var/www/!${APACHE_DOCUMENT_ROOT}!g" /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf



# X-SENDFILE
RUN echo [Enable Apache XSendfile]
RUN echo "XSendFile On\nXSendFilePath /user-files" | tee "/etc/apache2/conf-available/filerun.conf"
RUN a2enconf filerun
# X-SENDFILE


#### OLD COMPOSER INSTALLER #####
#RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'c5b9b6d368201a9db6f74e2611495f369991b72d9c8cbd3ffbc63edff210eb73d46ffbfce88669ad33695ef77dc76976') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
#RUN php composer-setup.php
#RUN php -r "unlink('composer-setup.php');"
#RUN mv composer.phar /composer
#RUN echo "alias composer='/composer'" >> ~/.bashrc
#### OLD COMPOSER INSTALLER #####



## REPO BASED INSTALLER
#RUN wget https://gitlab.com/azizyus/laravel-docker-base-example/-/raw/master/composer-install.sh
#RUN chmod +x ./composer-install.sh && ./composer-install.sh

### OFFICAL COMPOSER INSTALLER
### DOC https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md

RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -O - -q | php -- --quiet
RUN mv composer.phar /composer
RUN echo "alias composer='/composer'" >> ~/.bashrc

RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
