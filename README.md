# what does it do?

When you need to develop a project with laravel at least you need mysql and php so
this repo provides very entry level develop environment with docker


## how do i reach to project or phpmyadmin?

after the docker-composer up you should able to reach with these;
<br> project => localhost
<br> phpmyadmin => localhost:9090 (id pass is root)
<br> mysql => mysql (yes you need to write mysql instead of localhost)
<br> selenium => no idea but probably i will make it work when i understand it


## how am i gonna run php artisan commands from terminal if i cant reach to container?

As you can see there is an app: namespace or something like that in docker-compose.yml file and there is 
parameter which is called "working_dir:" and it points same places with php-service,
so you can write <code>docker-compose exec app php migrate</code> and it will be executed in the dir which is
i already specified as working_dir: 

